/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 15:36:16 by toto              #+#    #+#             */
/*   Updated: 2019/06/28 00:18:45 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <get_next_line.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>

# define __unused __attribute__((unused))

int     locate(const char *str, int c)
{
    const char *c_loc = ft_strchr(str, c);

    return ((c_loc != NULL) ? c_loc - str : 0);
}

void    append(char **dst, char *src)
{
    char *p_tmp;

    if (*dst == NULL)
        *dst = ft_strdup(src);
    else
    {
        p_tmp = *dst;
        *dst = ft_strjoin(*dst, src);
        free(p_tmp);
    }
}

void    cut(char **dst, int offset)
{
    char *p_tmp;

    if (*dst == NULL)
        return;
    p_tmp = *dst;
    *dst = ft_strdup(*dst + offset);
    free(p_tmp);
}

int     get_next_line(int fd, char **line)
{
    static char     *dyn_buffer = NULL;
    char            buffer[BUF_SIZE + 1];
    int             offset;
    int             bytes;

    while ((bytes = read(fd, buffer, BUF_SIZE)))
    {
        buffer[bytes] = '\0';
        append(&dyn_buffer, buffer);
        offset = locate(dyn_buffer, NEWLINE);
        
        if (offset > 0)
        {
            *line = ft_strndup(dyn_buffer, offset);
            cut(&dyn_buffer, offset + 1);
            return (SUCCESS);
        }
    }
    return (END);
}

int     main(int argc, char **argv)
{
    if (argc == 2)
    {
        char *line = NULL;
        int fd = open(argv[1], O_RDONLY);

        while (get_next_line(fd, &line))
            printf("Line : %s\n", line);
    }
    return (0);
}
