/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 16:20:55 by toto              #+#    #+#             */
/*   Updated: 2019/06/21 16:32:43 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef GET_NEXT_LINE
# define GET_NEXT_LINE

# define BUF_SIZE   24
# define NEWLINE    '\n'
# define SUCCESS    1
# define ERROR      -1
# define END        0
# define TRUE       1
# define FALSE      0

# endif
