# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: toto <marvin@42.fr>                        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/21 15:37:12 by toto              #+#    #+#              #
#    Updated: 2019/06/27 09:19:43 by toto             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC 			= gcc
FLAGS 		= -Wall -Werror -Wextra -g
INCLUDE 	= include
NAME		= get_next_line
DEBUGGER 	= lldb
TEST_FILE	= test/file.txt
LIBPATH		= static
LIBNAME		= ft

.PHONY: all

all:
	$(CC) $(FLAGS) -o $(NAME) $(NAME).c -I $(INCLUDE) -L$(LIBPATH) -l$(LIBNAME)

memsafe:
	$(CC) $(FLAGS) -o $(NAME) $(NAME).c -I $(INCLUDE) -L$(LIBPATH) -l$(LIBNAME) -fsanitize=address

clean:
	rm -rf $(NAME) $(NAME).dSYM

test: all
	./$(NAME) $(TEST_FILE)

debug: all
	$(DEBUGGER) $(NAME) $(TEST_FILE)

segdeb: all
	$(DEBUGGER) -- $(NAME) $(TEST_FILE)

re: clean all
